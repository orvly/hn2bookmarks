"use strict";

function log(textarea, text) {
    console.log(text);
    textarea.value += text + "\n";
    // scroll the text area to show the next text
    textarea.scrollTop = textarea.scrollHeight;
}
const domParser = new DOMParser();

// This will be injected into the current Hacker News and will
// actually perform the fetch, overcoming CORS problems of doing
// it from the background js (or asking for a permission).

(function () {
    /**
     * Check and set a global guard variable.
     * If this content script is injected into the same page again,
     * it will do nothing next time.
     */
    if (window.hasRun) {
        return;
    }
    window.hasRun = true;

    const port = chrome.runtime.connect({ name: "nhBookmarks" });
    let dialog = null;
    let logElement = null;
    let closeButton = null;

    // This runs in the page context, which allows us to fetch pages
    // from HN without violating CORS.
    function fetchPage(pathUrl, id) {
        const url = window.location.origin + pathUrl;
        log(logElement, url);

        const responsePromise = fetch(url, { method: "GET" });
        responsePromise.then(response => {
            if (response.ok) {
                return response.text();
            }
            else {
                if (response.status == 503) {
                    log(logElement, `${pathUrl} - HN refused, will back off and retry\n`);
                    port.postMessage({ nhMessageType: "throttleRequests", url: pathUrl, id: id });
                } else {
                    log(logElement, `${pathUrl} - unknown failure, status: ${response.status}, stopping.`);
                    port.postMessage({ nhMessageType: "stop" });
                    closeDialog();
                }
                return null;
            }
        }).then(text => {
            if (text !== null) {
                // Send page text to be processed in background
                port.postMessage({
                    nhMessageType: "collectBookmarksFromPage",
                    htmlText: text,
                    id: id
                });
            }
        });
    }
    function closeDialog() {
        dialog.style.display = "none";
        document.body.removeChild(dialog);
        dialog = null;
    }
    function onMessage(message) {
        if (message.command === "fetch") {
            console.log("WAITING " + message.throttleSleepTimeMs);

            // Call fetchPage only after the timeout we were sent, in order 
            // to not burden the HN server
            setTimeout(() => fetchPage(message.url, message.id), message.throttleSleepTimeMs);
        } else if (message.command === "getUsername") {
            const usernameLink = document.querySelector("a#me");
            if (usernameLink !== null) {
                const username = document.querySelector("a#me").innerText;
                port.postMessage({ nhMessageType: "username", username: username });
            } else {
                document.querySelector("#loginErrorMsg").style.display = "block";
                document.querySelector("#downloadingMsg").style.display = "none";
                closeButton.innerText = "Close";
            }
        }
        else if (message.command === "injectModal") {
            if (dialog === null) {
                const div = document.createElement("div");
                const parsed = domParser.parseFromString(message.dialogHtml, "text/html");
                div.appendChild(parsed.body);
                dialog = document.body.appendChild(div);
                closeButton = document.querySelector("#stopButton");
                // Register to the STOP button event
                closeButton.addEventListener("click", (e) => {
                    port.postMessage({ nhMessageType: "stop" });
                    closeDialog();
                });
            }
            logElement = dialog.querySelector("#nhFetchLog");
            dialog.style.display = "block";
        }
        else if (message.command === "finished") {
            console.log("FINISHED FETCHING!");
            closeDialog();
        }
    }
    /**
     * Listen for messages from the background script.
     */
    port.onMessage.addListener(onMessage);

})();
