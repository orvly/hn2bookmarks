﻿# Hacker News 2 Bookmarks

## What it does
Have you been using [Hacker News](https://news.ycombinator.com/news) as a sort of bookmark manager? Why not combine it with the one in your browser?

Have you ever wanted to ~~hoard~~ keep a local copy of all those cool links you liked on Hacker News, so you could ~~randomally click them when you're bored 🥱~~ research them on your spare time?  But clicking and scrolling through you liked history is such a drag...

Have you ever upvoted 🔼 a comment just to save that link in it for later browsing but forgot about it?

Never fear!  HN 2 bookmarks 🔖 will pull them into your browser's local bookmarks.  This is:

- A small **Firefox** extension which **scraps your submitted and/or liked HN links and turn them into local bookmarks**.
- Also it *optionally* scraps your submitted and/or liked **comments** for links and adds those links to your bookmarks.

## Technicalities
- Doesn't ask for your username and password
- You have to be signed in to HN, and the current tab has to be some HN page (not Algolia search) to start.
- Blocks the current tab for fetching, but you can continue browsing in other tabs.
- You have to give it the name of an existing bookmark folder
- Bookmarks from submissions will go in one folder, and bookmarks found in commments will got in another one.  They **can** be the same folder though.
- Links from comments **won't have a title**, since that's how HN shows them.  Might find a way to find their title if there's enough interest.
- Bookmarks **won't be added** if they already exist under either of those folders.
- Uses a (primitive but polite) backoff algorithm to avoid hammering the HN server with too many requests.
- Shows a primitive progress log, and can be stopped either by clicking a button or closing the tab.
- Saves settings for next time
- In my unscientific measurement, it took about 10 minutes to scrap into bookmarks around 8300 links from submissions + upvoted comments.

## Caveats
- This is something I threw together quickly, without really having any experience with javascript, html, css, or browser extensions, learning as I go.
- Firefox only for now. Didn't test it on Chrome, mainly because accessing bookmarks in Chrome right now looks very cumbersome, I think it's impossible to use async.  I think that soon, when Chrome will support manifest 3 it would be easier, but I'm not sure.
- Currently doesn't try to recover or restart, will always dig through history. Also doesn't try to bookmark just the new links from last time.  Might try to implement this if there's interest.
- Doesn't check if links are still valid, there are other extensions which does.

## Alternatives
- [Hacker News to Raindrop.io](https://github.com/davenicoll/hackernews-to-raindrop) - Python script to upload upvoted submissions only, directly to raindrops.io. Doesn't save them locally.

## How to build
- No need to build, this includes only plain javascript, HTML and CSS. No extra libraries were used or bundled.
