"use strict";

const settingsKey = "settings";
const BM = browser.bookmarks;

async function verifyBookmarkFolder(settingsObj, keyAndIdName) {
    let exists = false;
    const folderName = settingsObj[keyAndIdName];
    let bookmarks = await BM.search({ title: folderName });
    exists = bookmarks.length == 1 && bookmarks[0].type == "folder";
    const input = document.querySelector(`#${keyAndIdName}`);
    const errorMessageDiv = input.parentElement.querySelector(".errorNotFound");
    if (!exists) {
        input.classList.add("inputError");
        errorMessageDiv.classList.remove("hidden");
    } else {
        input.classList.remove("inputError");
        errorMessageDiv.classList.add("hidden");
    }
    return exists;
}

/**
 * Listen for clicks on the buttons, and send the appropriate message to
 * the content script in the page.
 */
function listenForClicks() {
    document.addEventListener("click", (e) => {

        async function run(tabs) {
            // Verify again that current tab is on HN
            const url = new URL(tabs[0].url);
            if (url.hostname !== "news.ycombinator.com") {
                reportExecuteScriptError("Should run this if the current tab is on Hacker News");
                return;
            }

            // Save settings before running
            const settingsSubObj = {};
            const settingsObj = {};
            settingsObj[settingsKey] = settingsSubObj;
            const inputs = document.querySelectorAll(".settings input");
            for (const inp of inputs) {
                if (inp.type == "checkbox") {
                    settingsSubObj[inp.id] = inp.checked;
                } else {
                    settingsSubObj[inp.id] = inp.value;
                }
            }
            await browser.storage.local.set(settingsObj);

            // Verify folders exist
            let settingsValid = true;
            if (settingsSubObj["doMySubmissions"] || settingsSubObj["doUpvotedSubmissions"]) {
                settingsValid = await verifyBookmarkFolder(settingsSubObj, "folderNameSubmissions") && settingsValid;
            } else {
                settingsSubObj["folderNameSubmissions"] = null;
            }
            if (settingsSubObj["doMyComments"] || settingsSubObj["doUpvotedComments"]) {
                settingsValid = await verifyBookmarkFolder(settingsSubObj, "folderNameComments") && settingsValid;
            } else {
                settingsSubObj["folderNameComments"] = null;
            }

            if (settingsValid) {
                const dialogHtml = document.querySelector("#progressDialog");
                // Send a message to the background script to begin running
                browser.runtime.sendMessage({
                    nhMessageType: "start",
                    settings: settingsSubObj,
                    dialogHtml: dialogHtml.outerHTML
                });
                window.close();
            }
        } // end of run function

        //  Get the active tab, and call run.
        if (e.target.classList.contains("go_button")) {
            browser.tabs.query({ active: true, currentWindow: true })
                .then(run);
        }
    });
}

/**
 * There was an error executing the script.
 * Display the popup's error message, and hide the normal UI.
 */
function reportExecuteScriptError(error) {
    document.querySelector("#popup-content").classList.add("hidden");
    document.querySelector("#error-content").classList.remove("hidden");
}


function startup(tabs) {
    const url = new URL(tabs[0].url);
    if (url.hostname !== "news.ycombinator.com") {
        reportExecuteScriptError("Not on hacker news");
        return;
    }
    // User is looking at hacker news, 
    // Show the content and hide the error
    document.querySelector("#popup-content").classList.remove("hidden");
    document.querySelector("#error-content").classList.add("hidden");

    // Build a hash from ID to input
    const inputs = document.querySelectorAll(".settings input");
    let inputsByName = {};
    for (const inp of inputs) {
        inputsByName[inp.id] = inp;
    }

    // Define defaults for settings, if some or all of them
    // won't be found in storage
    inputsByName["folderNameSubmissions"].value = null;
    inputsByName["folderNameComments"].value = null;
    inputsByName["doMySubmissions"].checked = true;
    inputsByName["doUpvotedSubmissions"].checked = true;
    inputsByName["doMyComments"].checked = false;
    inputsByName["doUpvotedComments"].checked = false;

    // Fill settings from storage
    browser.storage.local.get(settingsKey)
        .then((settings) => {
            if (!settings.hasOwnProperty(settingsKey)) {
                return;
            }
            const settingObj = settings[settingsKey];
            // Fill GUI from settings
            for (const keyName in settingObj) {
                if (inputsByName.hasOwnProperty(keyName)) {
                    if (inputsByName[keyName].type == "checkbox") {
                        inputsByName[keyName].checked = settingObj[keyName];
                    } else {
                        inputsByName[keyName].value = settingObj[keyName];
                    }
                }
            }
        });

    /**
     * When the popup loads, and current tab is hacker news,
     * add a click handler.
     * If we couldn't inject the script, handle the error.
     */
    browser.tabs.executeScript({ file: "/content_scripts/fetcher.js" })
        .then(listenForClicks)
        .catch(reportExecuteScriptError);
}

browser.tabs.query({ active: true, currentWindow: true })
    .then(startup)
    .catch(reportExecuteScriptError);
