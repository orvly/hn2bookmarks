"use strict";

const BM = browser.bookmarks;
const settingsKey = "settings";
let settings = {};
const rootFolders = {
    submissions: null,
    comments: null
};
let messagePort;
let username = null;
const existingBookmarks = new Set();
const domParser = new DOMParser();
let isRunning = false;
// We start up to 4 concurrent fetches (my submissions / upvoted submissions / 
// links from my comments / links from upvoted comments), and this keeps
// track if fetches are still ongoing. If all fethes have ended we can
// finish.
const hasPendingFetch = [false, false, false, false];

// Backoff / throttling
const baseThrottleSleepTimeMs = 800;
const baseBackoffFibonacciIndex = 2;  // Fib(2) is 1 and we want to start from a factor of 1
let currentBackoffFibonacciIndex = baseBackoffFibonacciIndex;
const fetchesBeforeDecreasingBackoff = 5;
let fetchBackoffCountdown = fetchesBeforeDecreasingBackoff;
let throttleSleepTimeMs = baseThrottleSleepTimeMs;

// Limit fetches - used during development, and also
// as a kind of primitive defence against bombarding HN
// with too many requests if we have a bug.
let numFetch = 0;
const maxFetches = 1000;

function addKnownBookmarks(folder) {
    for (const child of folder.children) {
        if (child.type == "bookmark") {
            existingBookmarks.add(child.url);
        } else if (child.type == "folder") {
            addKnownBookmarks(child);
        }
    }
}

async function collectExistingBookmarksUnder(folderName, rootFolderPropName) {
    const bookmarks = await BM.search({ title: folderName });
    if (bookmarks.length == 1 && bookmarks[0].type == "folder") {
        const roots = await BM.getSubTree(bookmarks[0].id);
        // Save the root folder we've found
        rootFolders[rootFolderPropName] = roots[0];
        // Collect bookmarks recursively
        addKnownBookmarks(roots[0]);
    }
    else {
        // We shouldn't get here since folder name should have been checked
        // by the GUI code before starting to run.
    }
}

async function collectExistingBookmarks() {
    existingBookmarks.clear();
    const nameOfFolderForSubmissions = settings["folderNameSubmissions"];
    const nameOfFolderForComments = settings["folderNameComments"];
    if (nameOfFolderForSubmissions !== null) {
        await collectExistingBookmarksUnder(nameOfFolderForSubmissions, "submissions");
    }
    if (nameOfFolderForComments == nameOfFolderForSubmissions) {
        rootFolders.comments = rootFolders.submissions;
    }
    else if (nameOfFolderForComments !== null) {
        await collectExistingBookmarksUnder(nameOfFolderForComments, "comments");
    }
    console.log("FOUND " + existingBookmarks.size + " existing bookmarks");
}

function fixUrl(href) {
    const url = new URL(href);
    // Links which refer back to HN are returned by the 'href' property
    // with a "moz-extension" protocol, change it back to the HN hostname.
    if (url.protocol === "moz-extension:") {
        url.protocol = "https:";
        url.hostname = "news.ycombinator.com";
    }
    return url;
}

function parseAndAddBookmarks(htmlText, id) {
    const links = [];
    {
        const dom = domParser.parseFromString(htmlText, "text/html");
        // Find out the URL in the "more" link and send a collect command to the
        // page to be run while we parse the bookmarks.
        const moreLink = dom.querySelector("a.morelink");
        if (moreLink !== null && numFetch++ < maxFetches) {
            //console.log("Found MORE link " + moreLink.pathname + moreLink.search);
            messagePort.postMessage({
                command: "fetch",
                url: moreLink.pathname + moreLink.search,
                throttleSleepTimeMs: throttleSleepTimeMs,
                id: id
            });
        } else {
            // No "More" link encountered in the current page OR
            // we have passed our maximum fetches
            hasPendingFetch[id] = false;
            if (hasPendingFetch.every(x => !x)) {
                // No more links encountered by all ongoing fetches, 
                // close the dialog
                messagePort.postMessage({ command: "finished" });
            }
        }

        // Collect all links
        let queryString = null;
        let rootFolder = null;

        if (dom.title.includes("comments")) {
            queryString = "span.commtext a[rel=nofollow]";
            rootFolder = rootFolders.comments;
        }
        else if (dom.title.includes("submissions")) {
            queryString = "a.titlelink";
            rootFolder = rootFolders.submissions;
        }
        if (queryString !== null) {
            const titleLinks = dom.querySelectorAll(queryString);
            //console.log("FOUND " + titleLinks.length + " LINKS");
            for (const hrefTitle of titleLinks) {
                const url = fixUrl(hrefTitle.href);
                if (!existingBookmarks.has(url)) {
                    links.push({title: hrefTitle.text, url: url.href, parentId: rootFolder.id });
                }
            }
        }
    }

    // Add all links as bookmarks
    for (const link of links) {
        BM.create(link);
        existingBookmarks.add(link.url);
    }
}

const sqrt5 = Math.sqrt(5);

// Use Fibonacci sequence for backoff.  Inspired by 
// https://www.npmjs.com/package/backoff
// Function below uses Binet's Fibonacci number formula, from:
// See https://mathworld.wolfram.com/FibonacciNumber.html
function fib(n) {
    return (Math.pow(1 + sqrt5, n) - Math.pow(1 - sqrt5, n)) / (Math.pow(2, n) * sqrt5);
}

function getThrottleTime(n) {
    const fibN = fib(n);
    // If factor is 1 then keep it as 1, else use an extra factor on top
    // of the fib number, to spread out the delays a bit more than what is 
    // returned by fib.
    return baseThrottleSleepTimeMs * ((fibN == 1) ? 1 : (fibN * 4));
}

function startFetching() {
    isRunning = true;
    currentBackoffFibonacciIndex = baseBackoffFibonacciIndex;
    if (settings["doUpvotedSubmissions"]) {
        hasPendingFetch[0] = true;
        messagePort.postMessage({
            command: "fetch",
            url: `/upvoted?id=${username}`,
            throttleSleepTimeMs: throttleSleepTimeMs,
            // Id is used only to identify this fetch
            // so we would know it's over and when to stop.
            id: 0 
        });
    }
    if (settings["doMySubmissions"]) {
        hasPendingFetch[1] = true;
        messagePort.postMessage({
            command: "fetch",
            url: `/submitted?id=${username}`,
            throttleSleepTimeMs: throttleSleepTimeMs,
            id: 1
        });
    }
    if (settings["doUpvotedComments"]) {
        hasPendingFetch[2] = true;
        messagePort.postMessage({
            command: "fetch",
            url: `/upvoted?id=${username}&comments=t`,
            throttleSleepTimeMs: throttleSleepTimeMs,
            id: 2
        });
    }
    if (settings["doMyComments"]) {
        hasPendingFetch[3] = true;
        messagePort.postMessage({
            command: "fetch",
            url: `/threads?id=${username}`,
            throttleSleepTimeMs: throttleSleepTimeMs,
            id: 3
        });
    }
}

function decreaseSleep() {
    // Since we got back a proper response, the backoff must have been big enough,
    // continue using it for the next "fetchesBeforeDecreasingSleep" fetches,
    // and then decrease it.
    if (fetchBackoffCountdown > 0) {
        fetchBackoffCountdown -= 1;
    }
    if (currentBackoffFibonacciIndex > baseBackoffFibonacciIndex && fetchBackoffCountdown == 0) {
        currentBackoffFibonacciIndex -= 1;
        throttleSleepTimeMs = getThrottleTime(currentBackoffFibonacciIndex);
        console.log("Throttling down requests, new sleep = " + throttleSleepTimeMs);
    }
    if (fetchBackoffCountdown == 0) {
        fetchBackoffCountdown = fetchesBeforeDecreasingBackoff;
    }
}

function increaseSleepAndRetry(message) {
    currentBackoffFibonacciIndex += 1;
    throttleSleepTimeMs = getThrottleTime(currentBackoffFibonacciIndex);
    fetchBackoffCountdown = fetchesBeforeDecreasingBackoff;
    console.log("Throttling up requests, new sleep = " + throttleSleepTimeMs);
    // Retry the fetch using the new timeout
    hasPendingFetch[message.id] = true;
    messagePort.postMessage({
        command: "fetch",
        url: message.url,
        throttleSleepTimeMs: throttleSleepTimeMs,
        id: message.id
    });
}

// This function is async due to the use of the bookmarks API 
// called by it indirectly, since in one of the paths
// here we read all existing bookmarks before starting to fetch HN pages.
async function onMessage(message) {
    if (message.nhMessageType == "start") {
        numFetch = 0;
        settings = message.settings;

        messagePort.postMessage({
            command: "injectModal", dialogHtml: message.dialogHtml
        });
        await collectExistingBookmarks();
        // After we will get back the username from the page, we'll start fetching
        messagePort.postMessage({
            command: "getUsername"
        });
    } else if (message.nhMessageType == "username") {
        username = message.username;
        startFetching();
    } else if (message.nhMessageType == "collectBookmarksFromPage" && isRunning) {
        parseAndAddBookmarks(message.htmlText, message.id);

        // Since we got back a proper response, the backoff must have been big enough,
        // continue using it for the next "fetchesBeforeDecreasingSleep" fetches,
        // and then decrease it.
        decreaseSleep();
    }
    else if (message.nhMessageType == "throttleRequests") {
        increaseSleepAndRetry(message);
    }
    else if (message.nhMessageType == "stop") {
        isRunning = false;
        existingBookmarks.clear();
    }
}

function connected(p) {
    if (p.name == "nhBookmarks") {
        messagePort = p;
        messagePort.onMessage.addListener(onMessage);
    }
}

// Register for message from the popup menu
browser.runtime.onMessage.addListener(onMessage);
// Wait for message from page
browser.runtime.onConnect.addListener(connected);

